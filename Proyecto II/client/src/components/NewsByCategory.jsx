import { Fragment } from "react";
import { React, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { useHistory } from "react-router-dom";
import img from "./img/no-image.svg";
import Loading from "./Loading";
export default function NewsByCategory() {
  const location = useLocation();

  return (
    <Fragment>
      <div className="NewsByCategoryframework d-flex flex-column justify-content-center align-items-center">
        <News />
      </div>
    </Fragment>
  );
}
function News() {
  const colors = [
    "shrinkbuttonred",
    "shrinkbuttonblue",
    "shrinkbuttonyellow",
    "shrinkbuttonpurple",
    "shrinkbuttongreen",
  ];
  var history = useHistory();
  const [newsList, setNewsList] = useState([]);
  const location = useLocation();
  const actionLocation = location.state.action;
  const newsLocation = location.state.news;
  const textLocation = location.state.text;
  useEffect(() => {
    if (actionLocation === "Category") {
      const newsFilter = newsLocation.filter((news) => {
        console.log(news.itemcategory);
        const newsFound = news.itemcategory.filter((item) => {
          console.log(item);
          return item === textLocation;
        });
        return newsFound.length > 0;
      });
      console.log(newsFilter);
      setNewsList(newsFilter);
    } else {
      const newsFilter = newsLocation.filter((news) => {
        const newsFound = news.description
          .toLowerCase()
          .indexOf(textLocation.toLowerCase());
        const newsFound1 = news.title
          .toLowerCase()
          .indexOf(textLocation.toLowerCase());
        return newsFound > -1 || newsFound1 > -1;
      });
      console.log(newsFilter);
      setNewsList(newsFilter);
    }
  }, []);

  return newsList.length === 0 ? (
    <Loading />
  ) : (
    <Fragment>
      <div className="watchmain d-flex flex-row justify-content-center align-items-center">
        <div className="divimage"></div>
        <div className="watchframework border">
          <div className="rounded">
            <h4 className="text-center m-3">
              {location.state.action}:{location.state.text}
            </h4>
          </div>
          <div className="buttondiv border m-1">
            <button
              className="backbutton text-center m-1"
              onClick={() => {
                history.push("/portal");
              }}
            >
              Back
            </button>
          </div>
          <div className="scrollNewsByCategory ">
            {newsList.map((item, index) => (
              <div key={index} className="card m-1 d-flex flex-row  ">
                {item.image == "" ? (
                  <div className=" align-self-center">
                    <a href={item.link} target="_blank">
                      <img src={img} className="newsimage " alt="..." />
                    </a>
                  </div>
                ) : (
                  <div className=" align-self-center">
                    <a href={item.link} target="_blank">
                      <img src={item.image} className="newsimage" alt="..." />
                    </a>
                  </div>
                )}

                <div className="card-body">
                  <div className="newsmain">
                    <h3 className="text-center">{item.title}</h3>
                    <p className="card-text">{item.description}</p>
                    <a href={item.link} target="_blank">
                      <button className="shrinkbuttonskyblue mt-2 mb-2">
                        Link
                      </button>
                    </a>
                    <h4> date</h4>
                    <p className="card-text">{item.date}</p>
                    <h4>Category</h4>
                    {item.itemcategory.length === 0 ? (
                      <p className="card-text">"..."</p>
                    ) : (
                      <div className="d-flex flex-row">
                        {item.itemcategory.slice(0, 5).map((item, index) => (
                          <button
                            disabled
                            key={index}
                            className={`${colors[index]} m-1`}
                            onClick={() => {
                              history.push({
                                pathname: "/newsbycategory",
                                search: "?query=hi!",
                                state: {
                                  news: newsList,
                                  category: item,
                                  action: "category",
                                },
                              });
                            }}
                          >
                            {item}
                          </button>
                        ))}
                      </div>
                    )}
                    <h4>Source</h4>
                    <p className="card-text">{item.source}</p> <h4>Creator</h4>
                    <p className="card-text">{item.creator}</p>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
}
