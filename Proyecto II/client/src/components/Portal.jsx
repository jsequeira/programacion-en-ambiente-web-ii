import { React, Fragment, useEffect, useState } from "react";
import * as mdb from "mdb-ui-kit"; // lib
import { useHistory } from "react-router-dom";
import "./css/style.css";
import axios from "axios";
import img from "./img/no-image.svg";
import Myresources from "./Myresources";
import MainNews from "./MainNews";
import Loading from "./Loading";
function Portal() {
  const GraphqlURL = "http://localhost:3003/graphql";

  const [newsList, setNewsList] = useState([]);
  const [categoriesList, setCategoriesList] = useState([]);
  const token = localStorage.getItem("token");
  const user = localStorage.getItem("userInfo");
  const jsonUser = JSON.parse(user);
  const userid = jsonUser._id;
  var config = {
    headers: {
      "x-access-token": token,
    },
  };
  const GraphqlQuery = `
  {
      getNews(userid:"${userid}") {
          userid
          resourceurl
          title
          description
          source
          creator
          link
          itemcategory
          resourcecategory
          image
          date
        }
        
      }
    `;
  /* -------------------------------------------------------------------------- */
  /*                                 categories                                 */
  /* -------------------------------------------------------------------------- */

  /* -------------------------------------------------------------------------- */
  /*                               cargar noticias                              */
  /* -------------------------------------------------------------------------- */
  useEffect(async () => {
    axios({
      url: GraphqlURL,
      method: "post",
      data: {
        query: GraphqlQuery,
      },
    }).then(async (response) => {
      if (response.data.data.getNews.length === 0) {
        alert("no hay noticias cargadas");
        await axios
          .post(
            "http://localhost:3001/news/loadnewstodatabase",
            { userid },
            config
          )
          .then(async (response) => {
            alert(response.data);
            setTimeout(function () {
              axios({
                url: GraphqlURL,
                method: "post",
                data: {
                  query: GraphqlQuery,
                },
              }).then(async (response) => {
                setNewsList(response.data.data.getNews);
                alert(`news:${response.data.data.getNews.length}`);
              });
              setInterval(async () => {
                await axios
                  .post(
                    "http://localhost:3001/news/updatedatabasebyid",
                    { userid },
                    config
                  )
                  .then((response) => {
                    console.log(response.data);
                  });
                console.log("30 segundos");
              }, 1000 * 30);
            }, 1000 * 30);
          });
      } else {
        setNewsList(response.data.data.getNews);
        alert(`news:${response.data.data.getNews.length}`);
        setInterval(async () => {
          await axios
            .post(
              "http://localhost:3001/news/updatedatabasebyid",
              { userid },
              config
            )
            .then((response) => {
              console.log(response.data);
            });
          console.log("30 segundos");
        }, 1000 * 30);
      }
    });
    await axios
      .get(
        `http://localhost:3001/resource/getresourcescategorybyid/${userid}`,
        config
      )
      .then((response) => {
        setCategoriesList(response.data);
      });
  }, []);
  /* -------------------------------------------------------------------------- */
  /*                         funcion cambia las noticias                        */
  /* -------------------------------------------------------------------------- */
  async function newsChange(newsList1) {
    setNewsList(newsList1);
  }

  return newsList.length === 0 || categoriesList.length === 0 ? (
    <div className="portalmain d-flex flex-row justify-content-center align-items-center">
      <div>
        <h3>wait please...</h3>
        <Loading />
      </div>
    </div>
  ) : (
    <Fragment>
      <div className="portalmain d-flex flex-row justify-content-center align-items-center">
        <div className="divimage"></div>
        <div className="portalframework border">
          <ul className="nav nav-tabs mb-3 m-3" id="ex1" role="tablist">
            <li className="nav-item  " role="presentation">
              <a
                className="nav-link active "
                id="ex1-tab-1"
                data-mdb-toggle="tab"
                href="#ex1-tabs-1"
                role="tab"
                aria-controls="ex1-tabs-1"
                aria-selected="true"
              >
                My resources
              </a>
            </li>
            <li className="nav-item" role="presentation">
              <a
                className="nav-link"
                id="ex1-tab-2"
                data-mdb-toggle="tab"
                href="#ex1-tabs-2"
                role="tab"
                aria-controls="ex1-tabs-2"
                aria-selected="false"
              >
                Main
              </a>
            </li>
            {categoriesList.map((category, index) => (
              <li key={index} className="nav-item" role="presentation">
                <a
                  className="nav-link"
                  id={`${category}1`}
                  data-mdb-toggle="tab"
                  href={`#${category}`}
                  role="tab"
                  aria-controls={category}
                  aria-selected="false"
                >
                  {category}
                </a>
              </li>
            ))}
          </ul>

          <div className="MainNews tab-content" id="ex1-content">
            <Myresources newsList={newsList} newsChange={newsChange} />
            <MainNews newsList={newsList} />
            <AllNews category={categoriesList} newsList={newsList} />
          </div>
        </div>
      </div>
    </Fragment>
  );
}
function AllNews(props) {
  console.log(props.category);
  console.log(props.newsList);
  const history = useHistory();
  const colors = [
    "shrinkbuttonred",
    "shrinkbuttonblue",
    "shrinkbuttonyellow",
    "shrinkbuttonpurple",
    "shrinkbuttongreen",
  ];

  return props.category.map((category, index) => (
    <div
      className="tab-pane fade"
      id={category}
      role="tabpanel"
      aria-labelledby={`#${category}`}
    >
      <div className="mainframework">
        <div className="scrollmainnews ">
          {props.newsList.map((item, index) =>
            item.resourcecategory === category ? (
              <div key={index} className="card m-1 d-flex flex-row  ">
                {item.image == "" ? (
                  <div className=" align-self-center">
                    <a href={item.link} target="_blank">
                      <img src={img} className="newsimage " alt="..." />
                    </a>
                  </div>
                ) : (
                  <div className=" align-self-center">
                    <a href={item.link} target="_blank">
                      <img src={item.image} className="newsimage" alt="..." />
                    </a>
                  </div>
                )}

                <div className="card-body">
                  <div className="newsmain">
                    <h3 className="text-center">{item.title}</h3>
                    <p className="card-text">{item.description}</p>
                    <a href={item.link} target="_blank">
                      <button className="shrinkbuttonskyblue mt-2 mb-2">
                        Link
                      </button>
                    </a>
                    <h4> date</h4>
                    <p className="card-text">{item.date}</p>
                    <h4>Category</h4>
                    {item.itemcategory.length === 0 ? (
                      <p className="card-text">"..."</p>
                    ) : (
                      <div className="d-flex flex-row">
                        {item.itemcategory.slice(0, 5).map((item, index) => (
                          <button
                            key={index}
                            className={` m-1 ${colors[index]}`}
                            onClick={() => {
                              history.push({
                                pathname: "/newsbycategory",
                                search: "?query=hi!",
                                state: {
                                  news: props.newsList,
                                  text: item,
                                  action: "Category",
                                },
                              });
                            }}
                          >
                            {item}
                          </button>
                        ))}
                      </div>
                    )}
                    <h4>Source</h4>
                    <p className="card-text">{item.source}</p> <h4>Creator</h4>
                    <p className="card-text">{item.creator}</p>
                  </div>
                </div>
              </div>
            ) : null
          )}
        </div>
      </div>
    </div>
  ));
}
export default Portal;
