import { Fragment, React, useEffect, useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { Formik, Form, Field } from "formik";

export default function Myresources(props) {
  var history = useHistory();
  const token = localStorage.getItem("token");
  const user = localStorage.getItem("userInfo");
  const [listResources, setResources] = useState([]);
  const jsonUser = JSON.parse(user);
  const userid = jsonUser._id;
  console.log(userid);
  let config = {
    headers: {
      "x-access-token": token,
    },
  };
  /* -------------------------------------------------------------------------- */
  /*                                get resources                               */
  /* -------------------------------------------------------------------------- */

  useEffect(() => {
    passwordLessValidation();
    alert(`numero de noticias, ${props.newsList.length}`);
    axios
      .post("http://localhost:3001/resource/getresources/", { userid }, config)
      .then((response) => {
        setResources(response.data);
      });
  }, []);
  /* -------------------------------------------------------------------------- */
  /*                               delete resource                              */
  /* -------------------------------------------------------------------------- */
  function deleteResource(_id, url) {
    if (listResources.length > 1) {
      axios
        .delete(`http://localhost:3001/resource/deleteresource/`, {
          headers: {
            "x-access-token": token,
            _id,
            url,
            userid,
          },
        })
        .then((response) => {
          alert(response.data.message);
          setResources(
            listResources.filter((resource) => {
              return resource._id !== _id;
            })
          );
          console.log("antes", props.newsList);
          const setNew = props.newsList.filter((news) => {
            return news.resourceurl !== url;
          });
          props.newsChange(setNew);
          console.log("despues", setNew);
        });
    } else {
      alert("Add other resource, please!");
    }
  }

  /* -------------------------------------------------------------------------- */
  /*                               update resource                              */
  /* -------------------------------------------------------------------------- */
  function updateResourse(resource) {
    console.log(resource);
    var idResource = resource._id;
    var name = prompt("Name", resource.name);
    var category = prompt("Category", resource.category);
    var url = prompt("url", resource.url);

    const newResource = {
      _id: idResource,
      userid: userid,
      name: name,
      category: category,
      url: url,
    };
    alert(JSON.stringify(resource));
    if (name === "" || category === "" || url === "") {
      alert("complete all field!");
    } else if (name === null || category === null || url === null) {
      console.log("no pasa nada amigo");
    } else {
      axios
        .put(
          "http://localhost:3001/resource/updateresource/",
          newResource,
          config
        )
        .then((response) => {
          alert(response.data);
          setResources(
            listResources.map((resource) => {
              console.log("sin modificar" + JSON.stringify(resource));
              return resource._id === idResource ? newResource : resource;
            })
          );
          console.log("cambiado" + JSON.stringify(listResources[0]));
        });
    }
  }
  /* -------------------------------------------------------------------------- */
  /*                                Search Formik                               */
  /* -------------------------------------------------------------------------- */
  function onSubmit(data) {
    if (data.text.length !== 0) {
      const newsFilter = props.newsList.filter((news) => {
        const newsFound = news.description
          .toLowerCase()
          .indexOf(data.text.toLowerCase());
        const newsFound1 = news.title
          .toLowerCase()
          .indexOf(data.text.toLowerCase());
        return newsFound > -1 || newsFound1 > -1;
      });

      if (newsFilter.length > 0) {
        history.push({
          pathname: "/newsbycategory",
          search: "?query=hi!",
          state: {
            news: props.newsList,
            text: data.text,
            action: "Search",
          },
        });
      } else {
        alert("No results");
      }
    } else {
      alert("empty field");
    }
  }
  const initialValues = {
    text: "",
  };

  /* -------------------------------------------------------------------------- */
  /*                                passwordless                                */
  /* -------------------------------------------------------------------------- */
  function passwordless() {
    if (document.getElementById("cb").checked === true) {
      axios
        .post("http://localhost:3001/user/chancepasswordless", {
          userid: jsonUser._id,
          boolean: true,
        })
        .then(() => {
          axios.post("http://localhost:3001/system/sendcodepasswordless", {
            userid: jsonUser._id,
            email: jsonUser.email,
          });
        });
      jsonUser.passwordless = true;
      localStorage.setItem("userInfo", JSON.stringify(jsonUser));
    } else {
      axios.post("http://localhost:3001/user/chancepasswordless", {
        userid: jsonUser._id,
        boolean: false,
      });
      jsonUser.passwordless = false;
      localStorage.setItem("userInfo", JSON.stringify(jsonUser));
    }
  }
  function passwordLessValidation() {
    if (jsonUser.passwordless === true) {
      const toggle = (document.getElementById("cb").checked = true);
    } else {
      const toggle = (document.getElementById("cb").checked = false);
    }
  }
  function sendnewlink() {
    if (document.getElementById("cb").checked === true) {
      axios
        .post("http://localhost:3001/system/sendcodepasswordless", {
          userid: jsonUser._id,
          email: jsonUser.email,
        })
        .then((res) => {
          alert("new link");
        });
    } else {
      alert("active passwordless");
    }
  }
  return (
    <Fragment>
      <div
        className="tab-pane fade show active "
        id="ex1-tabs-1"
        role="tabpanel"
        aria-labelledby="ex1-tab-1"
      >
        <div className=" d-flex align-items-center flex-column justify-content-center m-3">
          <Formik onSubmit={onSubmit} initialValues={initialValues}>
            <Form className="row g-3 m-0  w-100">
              <div className="col-auto  w-50">
                <Field
                  type="text"
                  className="form-control "
                  id="inputPassword2"
                  placeholder="News"
                  name="text"
                />
              </div>
              <div className="col-auto  w-25">
                <button type="submit" className="outlinebutton">
                  Search
                </button>
              </div>
            </Form>
          </Formik>
        </div>
        <div className="ml-2 toggle-div p-1">
          <label className="switch ml-4">
            Passwordless
            <input
              className="ml-2"
              id="cb"
              type="checkbox"
              onClick={() => {
                passwordless();
              }}
            />
          </label>
          <button
            className="ml-5  newbutton"
            onClick={() => {
              sendnewlink();
            }}
          >
            new link
          </button>
        </div>
        <table className="table table-striped table-borderless table-hover table-responsive-lg">
          <caption className=" m-3 ">
            <button
              className="newbutton ml-1 "
              onClick={() => {
                history.push("/newsource");
              }}
            >
              Add
            </button>
          </caption>
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Category</th>
              <th scope="col">URL</th>
              <th scope="col"> Edit</th>
              <th scope="col">Delete</th>
              <th scope="col">Watch</th>
            </tr>
          </thead>
          <tbody>
            {listResources.map((resource, index) => (
              <tr key={resource._id}>
                <th scope="row">{index + 1}</th>

                <td>{resource.name}</td>
                <td>{resource.category}</td>
                <td>{resource.url}</td>
                <td>
                  <button
                    className="editbutton"
                    onClick={() => {
                      updateResourse(resource);
                    }}
                  >
                    Edit
                  </button>
                </td>
                <td>
                  <button
                    className="btn-delete"
                    onClick={() => {
                      deleteResource(resource._id, resource.url);
                    }}
                  >
                    Delete
                  </button>
                </td>
                <td>
                  <button
                    className="watchbutton "
                    onClick={() => {
                      history.push({
                        pathname: "/watchresource",
                        search: "?query=hi!",
                        state: {
                          resourcename: resource.name,
                          news: props.newsList,
                        },
                      });
                    }}
                  >
                    Watch
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </Fragment>
  );
}
