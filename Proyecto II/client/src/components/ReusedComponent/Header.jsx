import { Fragment, React } from "react";
import logo from "./img/logo.ico";
import { Navbar, Nav, Dropdown } from "react-bootstrap";
import history, { useHistory } from "react-router-dom";
import "./css/Header.css";
export default function Header() {
  const url = new URL(window.location.href);
  console.log(url);
  return (
    //no poner border a header ni a main ni a footer
    <Fragment>
      <header>
        <Navbar className="navbar border-bottom" collapseOnSelect expand="lg">
          <Navbar.Brand>
            <Nav.Link className="Brand-link " href="/">
              <img src={logo} alt="" />
            </Nav.Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav  " />
          <Navbar.Collapse
            id="basic-navbar-nav "
            className="justify-content-end "
          >
            <Nav>
              <HeaderType />
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </header>
    </Fragment>
  );
}
function HeaderType() {
  const url = new URL(window.location.href);
  var history = useHistory();
  if (url.pathname === "/") {
    return (
      <Fragment>
        <Nav.Link className="link m-1 mr-4 " href="/login">
          Log in
        </Nav.Link>
        <Nav.Link className="link  m-1 mr-4 " href="/signup">
          Sign up
        </Nav.Link>
      </Fragment>
    );
  }
  if (url.pathname === "/portal") {
    return (
      <Fragment>
        <Nav.Link className=" m-1 mr-4 ">
          {JSON.parse(localStorage.getItem("userInfo")).firstname +
            " " +
            JSON.parse(localStorage.getItem("userInfo")).lastname}
        </Nav.Link>
        <Nav.Link
          className="link m-1 mr-4 "
          href="/"
          onClick={() => {
            localStorage.removeItem("userInfo");
            localStorage.removeItem("token");
          }}
        >
          Log out
        </Nav.Link>
      </Fragment>
    );
  }
  if (url.pathname === "/signup") {
    return (
      <Fragment>
        <Nav.Link className="link m-1 mr-4 " href="/login">
          Log in
        </Nav.Link>
      </Fragment>
    );
  }
  if (url.pathname === "/admimenu") {
    return (
      <Fragment>
        <Nav.Link className=" m-1 mr-4 ">
          {JSON.parse(localStorage.getItem("userInfo")).firstname +
            " " +
            JSON.parse(localStorage.getItem("userInfo")).lastname}
        </Nav.Link>
        <Nav.Link
          className="link m-1 mr-4 "
          href="/"
          onClick={() => {
            localStorage.removeItem("userInfo");
            localStorage.removeItem("token");
          }}
        >
          Log out
        </Nav.Link>
      </Fragment>
    );
  }
  if (url.pathname === "/login") {
    return (
      <Fragment>
        <Nav.Link className="link  m-1 mr-4 " href="/signup">
          Sign Up
        </Nav.Link>
      </Fragment>
    );
  }
  if (url.pathname === "/newsource") {
    return (
      <Fragment>
        <Nav.Link className=" m-1 mr-4  ">
          {JSON.parse(localStorage.getItem("userInfo")).firstname +
            " " +
            JSON.parse(localStorage.getItem("userInfo")).lastname}
        </Nav.Link>
        <Nav.Link
          className="link m-1 mr-4 "
          href="/"
          onClick={() => {
            localStorage.removeItem("userInfo");
            localStorage.removeItem("token");
          }}
        >
          Log out
        </Nav.Link>
      </Fragment>
    );
  } else {
    return (
      <Fragment>
        <Dropdown drop="left" className="navdropdownbutton  mr-4">
          <Dropdown.Menu>
            <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
            <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
            <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Fragment>
    );
  }
}
