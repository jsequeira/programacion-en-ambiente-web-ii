import { Fragment } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import Header from "./components/ReusedComponent/Header";
import Index from "./components/Index";
import Portal from "./components/Portal";
import SignUp from "./components/SignUp";
import LogIn from "./components/LogIn";
import Footer from "./components/ReusedComponent/Footer";
import NewSource from "./components/NewSource";
import ChooseResource from "./components/ChooseResource";
import watchResource from "./components/WatchResource";
import EmailValidate from "./components/validatecomponent/EmailValidate";
import PhoneNumberVatidate from "./components/validatecomponent/PhoneNumber";
import AdmiMenu from "./components/AdmiMenu";
import NewsByCategory from "./components/NewsByCategory";
import PasswordLess from "./components/PasswordLess";
function App() {
  return (
    //Siempre sustituir el div por el Frament
    //da problemas con height:100vh
    <Fragment>
      <Router>
        <Header />
        <Switch>
          <Route path="/" exact component={Index} />
          <Route path="/Signup" exact component={SignUp} />
          <Route path="/login" exact component={LogIn} />
          <Route path="/portal" exact component={Portal} />
          <Route path="/newsource" exact component={NewSource} />
          <Route path="/chooseresource" exact component={ChooseResource} />
          <Route path="/watchresource/" exact component={watchResource} />
          <Route path="/admimenu/" exact component={AdmiMenu} />{" "}
          <Route path="/emailvalidate/" exact component={EmailValidate} />{" "}
          <Route path="/PhoneNumber" exact component={PhoneNumberVatidate} />
          <Route path="/newsbycategory" exact component={NewsByCategory} />{" "}
          <Route path="/passwordless" exact component={PasswordLess} />
        </Switch>
      </Router>
      <Footer />
    </Fragment>
  );
}
//illustration:https://www.manypixels.co/gallery
//icons:https://iconos8.es/
export default App;
