const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();
const { graphqlHTTP } = require("express-graphql");
const schema = require("./schemas/NewsSchema");
const cors = require("cors");
const app = express();
app.use(cors());
app.use(
  "/graphql",
  graphqlHTTP({
    graphiql: true,
    schema: schema,
    context: {
      messageId: "test",
    },
  })
);

const db = process.env.DB;
const user = process.env.USER;
const pass = process.env.PASS;
const uri = `mongodb+srv://${user}:${pass}@cluster0.3xs0c.mongodb.net/${db}?retryWrites=true&w=majority`;
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
};
mongoose
  .connect(uri, options)
  .then(() => {
    console.log("DATABASE CONNECTED");
    app.listen(3003, () => {
      console.log("3003 SERVER CONNECTED");
    });
  })
  .catch((error) => {
    throw error;
  });
