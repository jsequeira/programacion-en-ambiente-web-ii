/* -------------------------------------------------------------------------- */
/*                                   package                                  */
/* -------------------------------------------------------------------------- */
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
require("dotenv").config();
const axios = require("axios");
let Parser = require("rss-parser");

/* -------------------------------------------------------------------------- */
/*                                vefify token                                */
/* -------------------------------------------------------------------------- */
const verifyToken = require("../verifyToken/verifyToken");
/* -------------------------------------------------------------------------- */
/*                                    model                                   */
/* -------------------------------------------------------------------------- */
const newsModel = require("../Model/news");
const resourcesModel = require("../Model/resources");
/* -------------------------------------------------------------------------- */
/*                                load resource                               */
/* -------------------------------------------------------------------------- */
router.post("/loadresource", verifyToken, async (req, res) => {
  const url = req.body.url;
  let parser = new Parser();
  (async () => {
    let feed = await parser.parseURL(url);
    res.send(feed.items);
  })();
});
/* -------------------------------------------------------------------------- */
/*                             get user resources                             */
/* -------------------------------------------------------------------------- */
const getUserResources = async (req, res, next) => {
  var id = req.body.userid;
  await resourcesModel.find({ userid: id }, async (err, result) => {
    if (err) {
      res.send(err);
    } else {
      var userResources = result;
      req.resource = { userid: id, userResources: userResources };
      next();
    }
  });
};
/* -------------------------------------------------------------------------- */
/*                           load news to database by id                      */
/* -------------------------------------------------------------------------- */
router.post(
  "/loadnewstodatabase",
  verifyToken,
  getUserResources,
  async (req, res) => {
    var userResources = req.resource.userResources;
    var userId = req.resource.userId;

    const loadresources = async () => {
      await userResources.map((userResources) => {
        let parser = new Parser();
        (async () => {
          var feed = await parser.parseURL(userResources.url);
          console.log("este,", userResources);
          feed.items.map((item) => {
            var image;
            var itemCategory;
            var creator;
            if (typeof item.enclosure === "undefined") {
              image = "";
            } else {
              image = item.enclosure.url;
            }
            if (typeof item.categories === "undefined") {
              itemCategory = [];
            } else {
              itemCategory = item.categories;
            }
            if (typeof item.creator === "undefined") {
              creator = "...";
            } else {
              creator = item.creator;
            }
            const news = newsModel({
              userid: userResources.userid,
              resourceurl: userResources.url,
              title: item.title,
              description: item.content,
              source: userResources.name,
              creator: creator,
              link: item.link,
              itemcategory: itemCategory,
              resourcecategory: userResources.category,
              image: image,
              date: item.isoDate,
            });
            (async () => {
              news.save();
            })().catch((err) => {
              console.error(err);
            });
          });
        })();
      });
      console.log("1");
      res.send("database updated");
    };
    await loadresources();
    console.log("2");
  }
);

/* -------------------------------------------------------------------------- */
/*                            delete news by userId                           */
/* -------------------------------------------------------------------------- */
router.post("/deletenewsbyuserId", async (req, res) => {
  const userId = req.body.userid;
  const news = await newsModel.deleteMany({ userid: userId }, (err, result) => {
    res.send("news deleted");
  });
});
/* -------------------------------------------------------------------------- */
/*                           delete newbyresourceurl                          */
/* -------------------------------------------------------------------------- */
router.delete("/DeleteNewsByResourceUrl", async (req, res) => {
  const resourseUrl = req.headers["resourceurl"]; //simepre en minuscula
  const userid = req.headers["userid"];
  console.log(resourseUrl);
  await newsModel.deleteMany(
    { resourceurl: resourseUrl, userid: userid },
    (err, result) => {
      console.log(result);
      res.send("news deleted");
    }
  );
});
/* -------------------------------------------------------------------------- */
/*                             get news by userid                             */
/* -------------------------------------------------------------------------- */
router.post("/getnewsbyuserid", verifyToken, async (req, res) => {
  const userid = req.body.userid;
  const news = await newsModel
    .find({ userid: userid }, (err, result) => {
      if (err) {
        res.send(err);
      } else {
        res.json(result);
      }
    })
    .sort({ date: -1 });
});

/* -------------------------------------------------------------------------- */
/*                               update database                              */
/* -------------------------------------------------------------------------- */
router.post(
  "/updatedatabasebyid",
  verifyToken,
  getUserResources,
  async (req, res) => {
    var userResources = req.resource.userResources;
    var userId = req.resource.userid;
    try {
      await newsModel.deleteMany({ userid: userId }, (err, result) => {
        if (err) {
          res.send("error al actualizar la base de datos");
        } else {
          console.log("news deleted");
        }
      });
      await userResources.map((userResources, index) => {
        let parser = new Parser();

        (async () => {
          try {
            var feed = await parser.parseURL(userResources.url);
            console.log("resource:", index + 1);
            console.log("news size:", feed.items.length);
            feed.items.map((item) => {
              var image;
              var itemCategory;
              if (typeof item.enclosure === "undefined") {
                image = "";
              } else {
                image = item.enclosure.url;
              }
              if (typeof item.categories === "undefined") {
                itemCategory = [];
              } else {
                itemCategory = item.categories;
              }
              if (typeof item.creator === "undefined") {
                creator = "...";
              } else {
                creator = item.creator;
              }
              const news = newsModel({
                userid: userResources.userid,
                resourceurl: userResources.url,
                title: item.title,
                description: item.content,
                source: userResources.name,
                creator: creator,
                link: item.link,
                itemcategory: itemCategory,
                resourcecategory: userResources.category,
                image: image,
                date: item.isoDate,
              });
              (async () => {
                news.save();
              })().catch((err) => {
                console.error(err);
              });
            });
          } catch (error) {
            console.log("error");
          }
        })();
      });

      res.send("news updated");
    } catch (e) {
      console.log("error por problemas de conexion");
    }
  }
);

module.exports = router;
