/* -------------------------------------------------------------------------- */
/*                                   package                                  */
/* -------------------------------------------------------------------------- */
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
require("dotenv").config();
/* -------------------------------------------------------------------------- */
/*                                vefify token                                */
/* -------------------------------------------------------------------------- */
const verifyToken = require("../verifyToken/verifyToken");
/* -------------------------------------------------------------------------- */
/*                                    Model                                   */
/* -------------------------------------------------------------------------- */
const resourcesModel = require("../Model/resources");
const newsModel = require("../Model/news");
/* -------------------------------------------------------------------------- */
/*                                save resource                               */
/* -------------------------------------------------------------------------- */
router.post("/saveresource", verifyToken, async (req, res, next) => {
  const { userid, name, category, url } = await req.body;
  console.log(userid, name, category, url);
  const resource = new resourcesModel({
    userid: userid,
    name: name,
    category: category,
    url: url,
  });

  resource.save();
  res.json({ id: resource._id, message: resource.name + "  saved" });
});

/* -------------------------------------------------------------------------- */
/*                                get resources                               */
/* -------------------------------------------------------------------------- */
router.post("/getresources", verifyToken, async (req, res) => {
  const { userid } = req.body;
  const resources = resourcesModel.find({ userid: userid }, (err, result) => {
    if (err) return res.send(err);
    res.json(result);
  });
});

/* -------------------------------------------------------------------------- */
/*                              delete resources                              */
/* -------------------------------------------------------------------------- */
router.delete("/deleteresource/", verifyToken, async (req, res) => {
  const { userid, _id, url } = req.headers;
  console.log(userid, _id, url);
  /*
  /* --------------------------- eleimina el recurso  -------------------------- */
  const resource = await resourcesModel.findByIdAndDelete({ _id });
  console.log(resource);
  await newsModel.deleteMany({ url, userid }, (err, result) => {
    res.json({ message: resource.name + " deleted" });
  });
});

/* -------------------------------------------------------------------------- */
/*                               update resource                              */
/* -------------------------------------------------------------------------- */
router.put("/updateresource", verifyToken, async (req, res) => {
  const _id = req.body._id;
  const resource = {
    name: req.body.name,
    category: req.body.category,
    url: req.body.url,
  };
  await resourcesModel.findByIdAndUpdate({ _id }, resource, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      (async () => {
        await newsModel.updateMany(
          {
            userid: result.userid,
            resourcecategory: result.category,
          },
          { resourcecategory: req.body.category }
        );
      })();
      res.json({ message: result.name + " updated" });
      console.log(result);
    }
  });
  /*
  const news = await newsModel.findOne({
    userid,
    resourcecategory: req.body.category,
  });
  await newsModel.updateMany(
    {
      userid,
      resourcecategory: news.resourcecategory,
    },
    { resourcecategory: req.body.category }
  );*/
});

/* -------------------------------------------------------------------------- */
/*                      find resources categories without repeating           */
/* -------------------------------------------------------------------------- */
router.get("/getresourcescategorybyid/:id", verifyToken, async (req, res) => {
  const userid = req.params.id;
  await resourcesModel.find({ userid }, (err, result) => {
    if (result) {
      const categories = result.map((resource) => {
        return resource.category;
      });
      const categoriesWithoutRepeating = categories.filter(
        (category, index) => {
          return categories.indexOf(category) === index;
        }
      );
      return res.json(categoriesWithoutRepeating);
    }
  });
});

module.exports = router;
