/* -------------------------------------------------------------------------- */
/*                                   package                                  */
/* -------------------------------------------------------------------------- */
const usersModel = require("../Model/users");
const { Router } = require("express");
const express = require("express");
const router = express.Router();
const nodemailer = require("nodemailer");
require("dotenv").config();
const axios = require("axios");
/* -------------------------------------------------------------------------- */
/*                              nodemailer config                             */
/* -------------------------------------------------------------------------- */
var transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.EMAIL,
    pass: process.env.EMAILPASSWORD,
  },
});

/* -------------------------------------------------------------------------- */
/*           send email-Activar siempre apliacaiones menos seguras de google  */
/* -------------------------------------------------------------------------- */
router.post("/sendemail", async (req, res) => {
  const email = req.body.email;
  const userid = req.body.userid;
  var mailOptions = {
    from: process.env.EMAIL,
    to: email,
    subject: "Email validation",
    text:
      "" +
      `https://email-phonenumber-validate.herokuapp.com/emailvalidate/${userid}`,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
      res.send("Email sent: " + info.response);
    }
  });
});

/* -------------------------------------------------------------------------- */
/*                            send email with code                            */
/* -------------------------------------------------------------------------- */
router.post("/sendcodepasswordless", async (req, res) => {
  const array = ["dsa", "asd", "zxc", "cxz", "vbn", "nbv"];

  const randomNumber = Math.round(Math.random() * (array.length - 1 - 0) + 0);
  console.log(randomNumber);
  const email = req.body.email;
  const userid = req.body.userid;
  var mailOptions = {
    from: process.env.EMAIL,
    to: email,
    subject: "Link passwordless",
    text:
      "" +
      `http://localhost:3000/passwordless/?${userid + array[randomNumber]}`,
  };

  transporter.sendMail(mailOptions, async function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
      await usersModel.findByIdAndUpdate(
        { _id: userid },
        { passwordlesscode: `${userid + array[randomNumber]}` }
      );
      res.send("Email sent: " + info.response);
    }
  });
});
/* -------------------------------------------------------------------------- */
/*                               send sms                                     */
/* -------------------------------------------------------------------------- */
router.post("/sendsms", (req, res) => {
  const accountSid = process.env.accountSid;
  const authToken = process.env.authToken;
  const client = require("twilio")(accountSid, authToken);
  client.messages
    .create({
      body: `https://email-phonenumber-validate.herokuapp.com/phonenumberValidate/${req.body.userid}`,
      to: `+506${req.body.phonenumber}`,
      from: "+14849093453",
    })
    .then((message) => {
      console.log(message.sid);
      res.send({ message: "sms sent" });
    })
    .done();
});
/* -------------------------------------------------------------------------- */
/*                                email confirm   existed                      */
/* -------------------------------------------------------------------------- */
router.post("/emailconfirm", async (req, res) => {
  const { email } = req.body;
  const user = await usersModel.findOne({ email });
  if (!user) return res.json({ auth: true });
  res.json({});
});

/* -------------------------------------------------------------------------- */
/*                                rss validate                                */
/* -------------------------------------------------------------------------- */
router.post("/rssvalidate", async (req, res) => {
  try {
    const { url } = req.body;
    const result = await axios(url);
    console.log(result);
    res.json({ message: true });
  } catch {
    console.log("error");
    res.json({});
  }
});
/* -------------------------------------------------------------------------- */
/*                               email validate                               */
/* -------------------------------------------------------------------------- */
router.post("/emailvalidate", async (req, res) => {
  const { userid } = req.body;
  const user = await usersModel.findOne({ _id: userid });
  if (!user) return res.json({ message: "Username does not exist" });
  return res.json({ emailvalidate: user.emailvalidate });
  res.json({});
});
router.post("/phonenumbervalidate", async (req, res) => {
  const { userid } = req.body;
  const user = await usersModel.findOne({ _id: userid });
  if (!user) return res.json({ message: "Username does not exist" });
  return res.json({ phonenumbervalidate: user.phonenumbervalidate });
  res.json({});
});
module.exports = router;
