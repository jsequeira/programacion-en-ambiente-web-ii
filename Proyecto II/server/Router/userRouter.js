/* -------------------------------------------------------------------------- */
/*                                  packages                                  */
/* -------------------------------------------------------------------------- */
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
require("dotenv").config();
/* -------------------------------------------------------------------------- */
/*                                vefify token                                */
/* -------------------------------------------------------------------------- */
const verifyToken = require("../verifyToken/verifyToken");
/* -------------------------------------------------------------------------- */
/*                                   models                                   */
/* -------------------------------------------------------------------------- */
const usersModel = require("../Model/users");

/* -------------------------------------------------------------------------- */
/*                                    signin                                  */
/* -------------------------------------------------------------------------- */
router.post("/signin", async (req, res) => {
  const { email, password } = req.body;

  /* ----------------- busca a un usuario por medio del email ----------------- */
  const user = await usersModel.findOne({ email: email });

  if (!user) {
    return res.json({ message: "the email doesnt exist" });
  }
  /* -------- compara las claves del usuario encontrado con el recibido ------- */
  const passwordValidate = await user.comparePassword(password);

  if (!passwordValidate) {
    return res.send({ message: "Incorrect password" });
  }
  /* ---------------------------- se crea el token ---------------------------- */
  const token = jwt.sign({ id: user._id }, process.env.TOKENACCESS, {
    expiresIn: 60 * 60 * 24,
  });

  /* ------------------ se envia datos del usuario y el token ----------------- */

  user.password = "...";

  res.send({ auth: true, token, user });
});

/* -------------------------------------------------------------------------- */
/*                                    signup                                   */
/* -------------------------------------------------------------------------- */
router.post("/signup", async (req, res) => {
  const { email, firstname, lastname, password, roles, phonenumber } = req.body;
  /* ---------------------------- se crea el modelo --------------------------- */
  const user = new usersModel({
    email: email,
    firstname: firstname,
    lastname: lastname,
    password: password,
    phonenumber: phonenumber,
    roles: [roles],
  });
  /* -------------------------- la clave se encripta -------------------------- */
  user.password = await user.encryptPassword(user.password);

  /* ----------------- se salva el usuario en la base de datos ---------------- */
  user.save();

  /* ---------------------------- se envia el auth --------------------------- */
  res.send({ auth: true, user: user });
});
/* -------------------------------------------------------------------------- */
/*                             chance passwordless                            */
/* -------------------------------------------------------------------------- */
router.post("/chancepasswordless", async (req, res, next) => {
  const updateduser = await usersModel.findByIdAndUpdate(
    { _id: req.body.userid },
    { passwordless: req.body.boolean },
    { new: true }
  );
  res.send(updateduser);
});
/* -------------------------------------------------------------------------- */
/*                        get user by passwordlesscode                        */
/* -------------------------------------------------------------------------- */
router.post("/getuserbypasscode", async (req, res) => {
  const { passwordlesscode } = req.body;
  const user = await usersModel.findOne({
    passwordlesscode: passwordlesscode,
  });
  if (!user) return res.json({ auth: false });

  const token = jwt.sign({ id: user._id }, process.env.TOKENACCESS, {
    expiresIn: 60 * 60 * 24,
  });

  /* ------------------ se envia datos del usuario y el token ----------------- */

  user.password = "...";
  res.json({ user: user, token, auth: true });
});
module.exports = router;
