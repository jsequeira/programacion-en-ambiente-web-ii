const jwt = require("jsonwebtoken");
const verifyToken = (req, res, next) => {
  const token = req.headers["x-access-token"];
  if (req.headers["_id"]) {
    req._id = req.headers["_id"];
    console.log("el id chavom", req._id);
  }
  if (!token) {
    return res.status(401).json({
      auth: false,
      message: "no token provided",
    });
  }
  const decoded = jwt.verify(token, process.env.TOKENACCESS);
  console.log("token", decoded);

  next();
};
module.exports = verifyToken;
