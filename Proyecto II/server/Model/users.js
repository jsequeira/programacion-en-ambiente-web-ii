var mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const { Schema } = mongoose;
const usersSchema = new mongoose.Schema({
  email: {
    type: String,
  },
  firstname: {
    type: String,
  },
  lastname: {
    type: String,
  },
  password: {
    type: String,
  },
  roles: {
    type: Array,
  },
  emailvalidate: {
    type: Boolean,
    default: false,
  },
  phonenumbervalidate: {
    type: Boolean,
    default: false,
  },
  phonenumber: {
    type: Number,
  },
  passwordless: {
    type: Boolean,
    default: false,
  },
  passwordlesscode: {
    type: String,
    default: "",
  },
});

usersSchema.methods.encryptPassword = async (password) => {
  const salt = bcrypt.genSaltSync(10);
  return bcrypt.hashSync(password, salt);
};

usersSchema.methods.comparePassword = function (password) {
  return bcrypt.compare(password, this.password);
};

const usersModel = mongoose.model("users", usersSchema);
module.exports = usersModel;
