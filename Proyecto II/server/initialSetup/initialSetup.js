const roleModel = require("../Model/roles");
const categoriesModel = require("../Model/caterories");
const rolesVerification = async () => {
  const roles = await roleModel.find({});
  if (roles.length === 0)
    return await roleModel.insertMany([{ name: "admin" }, { name: "user" }]);
};
const categoriesVerification = async () => {
  const categories = await categoriesModel.find({});
  if (categories.length === 0)
    return await categoriesModel.insertMany([
      { name: "General" },
      { name: "Sport" },
      { name: "Food" },
    ]);
};
module.exports = { rolesVerification, categoriesVerification };
