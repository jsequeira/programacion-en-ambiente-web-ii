import React from "react";

export default function post() {
  return (
    <div className="postmain d-flex flex-row justify-content-center align-items-center">
      <div className="postframework border">
        <div className="border rounded">
          <h1 className="text-center m-3">titulo</h1>
        </div>
        <div className="scrolldiv">
          <div className="card m-1 d-flex flex-row  ">
            <div className="card-body">
              <div className="postmain">
                <h3 className="text-center">sutitulo</h3>
                <p className="card-text">texto</p>
                <h4>link</h4>
                <p className="card-text">Some</p>
                <h4>fecha</h4>
                <p className="card-text">Some</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
