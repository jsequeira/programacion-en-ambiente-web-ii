const { makeExecutableSchema } = require("graphql-tools"); //se usa para unir el shema y el resolver
const resolvers = require("./resolvers.js");

const typeDefs = `
    type Query {
        hello:String
        miNombre(name:String!):String 
        tasks:[Tasks]
    }
    type Tasks{
        _id:ID
        title:String!
        description:String!
        number:Int

    }
`; //String! --> el campo debe ser obligatorio
//une el esquema con el resolvers
//el esquema es lo que puedo consutar y el resolver es como lo consulto
module.exports = makeExecutableSchema({
  typeDefs: typeDefs,
  resolvers: resolvers,
});
