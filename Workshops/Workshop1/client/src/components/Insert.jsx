import React from "react";
export default function Insert(props) {
  return (
    <div className="d-flex flex-column align-items-center border m-3">
      <h1>Insertar Cambio</h1>
      <button
        onClick={() => {
          props.addCambio();
        }}
        className="btn btn-primary m-3"
      >
        Insertar
      </button>
    </div>
  );
}
