import "./App.css";

import axios from "axios";
import { React, useState, useEffect, Fragment } from "react";

function App() {
  /* -------------------------------------------------------------------------- */
  /*                                     get                                    */
  /* -------------------------------------------------------------------------- */

  const [token, setToken] = useState({});
  const [cerveserias, setCerveserias] = useState([]);

  function sinToken() {
    axios.post("http://localhost:3001/getbreweries").then((response) => {});
    setCerveserias([]);
  }

  function conToken() {
    console.log("token que voy a enviar", token.token);
    let config = {
      headers: {
        "x-access-token": token.token,
      },
    };
    axios
      .post("http://localhost:3001/getbreweries", {}, config)
      .then((response) => {
        setCerveserias(response.data);
      });
  }
  useEffect(() => {
    axios.get("http://localhost:3001/").then((response) => {
      setToken(response.data);
      console.log(response.data);
    });
  }, []);
  return token === {} ? (
    <div>Loading...</div>
  ) : (
    <Fragment>
      <div className="App">
        <div>token:{token.token}</div>
        <div>
          <button
            onClick={() => {
              conToken();
            }}
          >
            Obtener cerveserias con jwt token
          </button>
        </div>
        <div>
          <button
            onClick={() => {
              sinToken();
            }}
          >
            Obtener cerveserias sin jwt token
          </button>
        </div>
        <div>
          name
          {cerveserias.map((value) => (
            <div key={value.id}>{value.name} </div>
          ))}
        </div>
      </div>
    </Fragment>
  );
}

export default App;
