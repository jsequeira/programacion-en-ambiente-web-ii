import Table from "./components/Table";

function App() {
  return (
    <div className="h-100 ">
      <Table />
    </div>
  );
}

export default App;
