import { React, Fragment, useEffect, useState } from "react";
import "../components/style.css";
import axios from "axios";

export default function Table() {
  const [products, setProducts] = useState([]);

  /* -------------------------------------------------------------------------- */
  /*                                get products                                */
  /* -------------------------------------------------------------------------- */

  useEffect(() => {
    axios.get("http://localhost:3001/products/getproducts").then((res) => {
      setProducts(res.data);
    });
  }, []);

  /* -------------------------------------------------------------------------- */
  /*                                   delete                                   */
  /* -------------------------------------------------------------------------- */
  function deleteProduct(id) {
    axios
      .delete(`http://localhost:3001/products/deleteproduct/${id}`)
      .then((res) => {
        alert(res.data);
        setProducts(
          products.filter((product) => {
            return product._id !== id;
          })
        );
      });
  }
  /* -------------------------------------------------------------------------- */
  /*                                    edit                                    */
  /* -------------------------------------------------------------------------- */
  function editProduct(product) {
    const id = product._id;
    const name = prompt("name", product.name);
    const quantities = prompt("name", product.quantities);

    if (
      name === null ||
      quantities === null ||
      name === "" ||
      quantities === ""
    ) {
      alert("no saved");
    } else {
      axios
        .put(`http://localhost:3001/products/updateproduct/${id}`, {
          name,
          quantities,
        })
        .then((res) => {
          alert(res.data);
          setProducts(
            products.map((product) => {
              return product._id === id
                ? {
                    _id: id,
                    name: name,
                    quantities: quantities,
                  }
                : product;
            })
          );
        });
    }
  }
  /* -------------------------------------------------------------------------- */
  /*                                     new                                    */
  /* -------------------------------------------------------------------------- */
  function newProduct() {
    const name = prompt("name");
    const quantities = prompt("quantities");
    if (
      name === null ||
      quantities === null ||
      name === "" ||
      quantities === ""
    ) {
      alert("no saved");
    } else {
      axios
        .post("http://localhost:3001/products/createproduct", {
          name,
          quantities,
        })
        .then((response) => {
          setProducts([
            ...products,
            { _id: response.data._id, name: name, quantities: quantities },
          ]);
        });
    }
  }
  return (
    <Fragment>
      <div className="main d-flex flex-column justify-content-center align-items-center">
        <h1>Products</h1>
        <div className="container">
          <table class="table table table-striped table-hover table-responsive-sm">
            <caption>
              {" "}
              <button
                className="btn btn-primary"
                onClick={() => {
                  newProduct();
                }}
              >
                New
              </button>
            </caption>
            <thead>
              <tr>
                <th scope="col">id</th>
                <th scope="col">name</th>
                <th scope="col">quantities</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
              </tr>
            </thead>
            <tbody>
              {products.map((product) => (
                <tr>
                  <th scope="row">{product._id}</th>
                  <td>{product.name}</td>
                  <td>{product.quantities}</td>
                  <td>
                    <button
                      className="btn btn-secondary"
                      onClick={() => {
                        editProduct(product);
                      }}
                    >
                      Edit
                    </button>
                  </td>
                  <td>
                    <button
                      className="btn  btn-info"
                      onClick={() => {
                        deleteProduct(product._id);
                      }}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </Fragment>
  );
}
