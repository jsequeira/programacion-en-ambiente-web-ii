import { React, useState, useEffect } from "react";
import axios from "axios";

function Table(props) {
  const [cursos, setCursos] = useState([]);
  //const [listOfFriends, setListOfFriends] = useState([]);
  /* -------------------------------------------------------------------------- */
  /*                                   agregar                                  */
  /* -------------------------------------------------------------------------- */
  const agregarCurso = () => {
    const name = prompt("Agregue nombre");
    const code = prompt("Agregue codigo");
    const career = prompt("Agregue carrera");
    const credits = prompt("Agregue creditos");
    axios
      .post("http://localhost:3001/add", {
        name: name,
        code: code,
        career: career,
        credits: credits,
      })
      .then((res) => {
        alert("Exitoso!");
        setCursos([
          ...cursos,
          {
            _id: res.data._id,
            name: name,
            code: code,
            career: career,
            credits: credits,
          },
        ]);
      });
  };
  /* -------------------------------------------------------------------------- */
  /*                                   update                                   */
  /* -------------------------------------------------------------------------- */
  const actualizarCurso = (id) => {
    const name = prompt("inserte el nuevo nombre");
    axios
      .put(`http://localhost:3001/update`, {
        name: name,
        id: id,
      })
      .then((res) => {
        setCursos(
          cursos.map((curso) => {
            alert(res.data);
            return curso._id === id
              ? {
                  _id: id,
                  name: name,
                  code: curso.code,
                  career: curso.career,
                  credits: curso.credits,
                }
              : curso;
          })
        );
      });
  };
  /* -------------------------------------------------------------------------- */
  /*                                   delete                                   */
  /* -------------------------------------------------------------------------- */
  const eliminar = (id) => {
    axios.delete(`http://localhost:3001/delete/${id}`).then(() => {
      setCursos(
        cursos.filter((val) => {
          return val._id !== id;
        })
      );
    });
  };
  /* -------------------------------------------------------------------------- */
  /*                                    leer                                    */
  /* -------------------------------------------------------------------------- */
  useEffect(() => {
    axios
      .get("http://localhost:3001/read")
      .then((response) => {
        setCursos(response.data);
      })
      .catch((e) => {
        alert(e);
      });
  }, []);
  return (
    <div>
      <div className="container d-flex justify-content-center align-items-center table-hover table-responsive-sm">
        <table className="table table-borderless  table-info table-striped caption-top">
          <caption>
            <button
              className="btn btn-primary"
              onClick={() => {
                agregarCurso();
              }}
            >
              Agregar
            </button>
          </caption>
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">name</th>
              <th scope="col">code</th>
              <th scope="col">career</th>
              <th scope="col">credits</th>
              <th scope="col">Acción</th>
            </tr>
          </thead>
          <tbody>
            {cursos.map((value, index) => (
              <tr key={value._id}>
                <td>{index + 1}</td>
                <td>{value.name}</td>
                <td>{value.code}</td>
                <td>{value.career}</td>
                <td>{value.credits}</td>
                <td>
                  <button
                    className="btn btn-primary m-1"
                    onClick={() => {
                      actualizarCurso(value._id);
                    }}
                  >
                    Editar
                  </button>
                  <button
                    className="btn btn-danger m-1"
                    onClick={() => {
                      eliminar(value._id);
                    }}
                  >
                    Eliminar
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default Table;
