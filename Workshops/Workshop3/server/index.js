const express = require("express");
require("dotenv").config();
const app = express();
const cors = require("cors");
const mongoose = require("mongoose");
const CursoModel = require("./models/Cursos");
app.use(cors());
app.use(express.json());
/* -------------------------------------------------------------------------- */
/*                          conexion a base de datos                         */
/* -------------------------------------------------------------------------- */
const user = process.env.DB_USER;
const password = process.env.DB_PASS;
const database = process.env.DB_NAME;
const uri = `mongodb+srv://${user}:${password}@cluster0.3xs0c.mongodb.net/${database}?retryWrites=true&w=majority`;
mongoose
  .connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Base de Datos conectada"))
  .catch((e) => console.log(e));
/* -------------------------------------------------------------------------- */
/*                                    leer                                    */
/* -------------------------------------------------------------------------- */
app.get("/read", async (req, res) => {
  CursoModel.find({}, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.json(result);
    }
  });
});
/* -------------------------------------------------------------------------- */
/*                                  eliminar                                  */
/* -------------------------------------------------------------------------- */
app.delete("/delete/:id", async (req, res) => {
  id = req.params.id;
  await CursoModel.findByIdAndRemove(id).exec();
  res.send("item deleted");
});
/* -------------------------------------------------------------------------- */
/*                                   agregar                                  */
/* -------------------------------------------------------------------------- */
app.post("/add", async (req, res, next) => {
  name = req.body.name;
  code = req.body.code;
  career = req.body.career;
  credits = req.body.credits;
  const curso = new CursoModel({
    name: name,
    code: code,
    career: career,
    credits: credits,
  });
  await curso.save();
  res.send(curso);
});
/* -------------------------------------------------------------------------- */
/*                                   actualizar                                   */
/* -------------------------------------------------------------------------- */
app.put("/update", async (req, res) => {
  const name = req.body.name;
  const id = req.body.id;
  try {
    CursoModel.findById(id, (error, cursoUpdate) => {
      cursoUpdate.name = String(name);
      cursoUpdate.save();
      res.send("exitoso!");
    });
  } catch (error) {
    console.log(error);
  }
});
const port = 3001;
app.listen(port, () => {
  console.log("Conectado");
});
