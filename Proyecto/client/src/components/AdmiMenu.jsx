import { React, Fragment, useState, useEffect } from "react";
import axios from "axios";
import Loading from "./Loading";
export default function AdmiMenu() {
  const [listCategories, setlistCategories] = useState([]);
  /* -------------------------------------------------------------------------- */
  /*                               get categories                               */
  /* -------------------------------------------------------------------------- */
  useEffect(() => {
    axios
      .post("http://localhost:3001/category/getcategories")
      .then((response) => {
        setlistCategories(response.data);
      });
  }, []);
  /* -------------------------------------------------------------------------- */
  /*                               delete category                              */
  /* -------------------------------------------------------------------------- */
  function deletecategory(idCategory) {
    alert(idCategory);
    axios
      .post("http://localhost:3001/category/deletecategory/", { idCategory })
      .then((response) => {
        alert(response.data);
        setlistCategories(
          listCategories.filter((category) => {
            return category._id != idCategory;
          })
        );
      });
  }
  /* -------------------------------------------------------------------------- */
  /*                               insert category                              */
  /* -------------------------------------------------------------------------- */
  function insertCategory() {
    const name = prompt("Category name");
    if (name === null) {
      alert("canceled");
    } else {
      axios
        .post("http://localhost:3001/category/insertcategory/", { name })
        .then((response) => {
          alert(response.data);
          setlistCategories([
            ...listCategories,
            { _id: response.data._id, name: name },
          ]);
        });
    }
  }
  /* -------------------------------------------------------------------------- */
  /*                                   update                                   */
  /* -------------------------------------------------------------------------- */
  function updateResourse(category) {
    console.log(category);
    var name = prompt("Name", category.name);
    const newCategory = {
      _id: category._id,
      name: name,
    };
    alert(JSON.stringify(newCategory));
    if (name === "") {
      alert("complete all field!");
    } else if (name === null) {
      console.log("no pasa nada amigo");
    } else {
      axios
        .post("http://localhost:3001/category/updatecategory/", newCategory)
        .then((response) => {
          alert(response.data);
          setlistCategories(
            listCategories.map((category) => {
              console.log("sin modificar" + JSON.stringify(category));
              return category._id === newCategory._id ? newCategory : category;
            })
          );
          console.log("cambiado" + JSON.stringify(listCategories[0]));
        });
    }
  }
  return listCategories.length === 0 ? (
    <div>
      <Loading />
    </div>
  ) : (
    <Fragment>
      <div className="portalmain d-flex flex-row justify-content-center align-items-center">
        {" "}
        <div className="portalframework border">
          {" "}
          <table className="table table-striped table-borderless table-hover table-responsive-lg">
            <caption className=" m-3 ">
              <button
                className="newbutton ml-1 "
                onClick={() => {
                  insertCategory();
                }}
              >
                Add
              </button>
            </caption>
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
              </tr>
            </thead>
            <tbody>
              {listCategories.map((category) => (
                <tr key={category._id}>
                  <th scope="row">1</th>
                  <td>{category.name}</td>

                  <td>
                    <button
                      className="editbutton"
                      onClick={() => {
                        updateResourse(category);
                      }}
                    >
                      Edit
                    </button>
                  </td>
                  <td>
                    <button
                      className="btn-delete"
                      onClick={() => {
                        deletecategory(category._id);
                      }}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </Fragment>
  );
}
