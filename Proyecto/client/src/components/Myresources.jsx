import { Fragment, React, useEffect, useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
export default function Myresources() {
  var history = useHistory();
  const token = localStorage.getItem("token");
  const user = localStorage.getItem("userInfo");
  const jsonUser = JSON.parse(user);
  const userid = jsonUser._id;
  console.log(userid);
  let config = {
    headers: {
      "x-access-token": token,
    },
  };
  /* -------------------------------------------------------------------------- */
  /*                                get resources                               */
  /* -------------------------------------------------------------------------- */
  const [listResources, setResources] = useState([]);
  useEffect(() => {
    axios
      .post("http://localhost:3001/resource/getresources/", { userid }, config)
      .then((response) => {
        setResources(response.data);
        console.log(listResources);
      });
  }, []);
  /* -------------------------------------------------------------------------- */
  /*                               delete resource                              */
  /* -------------------------------------------------------------------------- */
  function deleteResource(idResource) {
    axios
      .post(
        "http://localhost:3001/resource/deleteresource/",
        { idResource },
        config
      )
      .then((response) => {
        setResources(
          listResources.filter((resource) => {
            return resource._id != idResource;
          })
        );
      });
  }
  /* -------------------------------------------------------------------------- */
  /*                               update resource                              */
  /* -------------------------------------------------------------------------- */
  function updateResourse(resource) {
    console.log(resource);
    var idResource = resource._id;
    var name = prompt("Name", resource.name);
    var category = prompt("Category", resource.category);
    var url = prompt("url", resource.url);

    const newResource = {
      _id: idResource,
      userid: userid,
      name: name,
      category: category,
      url: url,
    };
    alert(JSON.stringify(resource));
    if (name === "" || category === "" || url === "") {
      alert("complete all field!");
    } else if (name === null || category === null || url === null) {
      console.log("no pasa nada amigo");
    } else {
      axios
        .post(
          "http://localhost:3001/resource/updateresource/",
          newResource,
          config
        )
        .then((response) => {
          alert(response.data);
          setResources(
            listResources.map((resource) => {
              console.log("sin modificar" + JSON.stringify(resource));
              return resource._id === idResource ? newResource : resource;
            })
          );
          console.log("cambiado" + JSON.stringify(listResources[0]));
        });
    }
  }

  return (
    <Fragment>
      <div
        className="tab-pane fade show active "
        id="ex1-tabs-1"
        role="tabpanel"
        aria-labelledby="ex1-tab-1"
      >
        <table className="table table-striped table-borderless table-hover table-responsive-lg">
          <caption className=" m-3 ">
            <button
              className="newbutton ml-1 "
              onClick={() => {
                history.push("/newsource");
              }}
            >
              Add
            </button>
          </caption>
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Category</th>
              <th scope="col">URL</th>
              <th scope="col"> Edit</th>
              <th scope="col">Delete</th>
              <th scope="col">Watch</th>
            </tr>
          </thead>
          <tbody>
            {listResources.map((resource) => (
              <tr key={resource._id}>
                <th scope="row">1</th>
                <td>{resource.name}</td>
                <td>{resource.category}</td>
                <td>{resource.url}</td>
                <td>
                  <button
                    className="editbutton"
                    onClick={() => {
                      updateResourse(resource);
                    }}
                  >
                    Edit
                  </button>
                </td>
                <td>
                  <button
                    className="btn-delete"
                    onClick={() => {
                      deleteResource(resource._id);
                    }}
                  >
                    Delete
                  </button>
                </td>
                <td>
                  <button
                    className="watchbutton "
                    onClick={() => {
                      history.push({
                        pathname: "/watchresource",
                        search: "?query=hi!",
                        state: {
                          detail: `${resource.url}`,
                          resourcename: `${resource.name}`,
                        },
                      });
                    }}
                  >
                    Watch
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </Fragment>
  );
}
