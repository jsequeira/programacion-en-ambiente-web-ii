import { useEffect, Fragment } from "react";
import { React, useState } from "react";
import { useLocation } from "react-router-dom";
import { useHistory } from "react-router-dom";
import axios from "axios";
import img from "./img/no-image.svg";
import Loading from "./Loading";
export default function WatchResource() {
  var history = useHistory();
  const location = useLocation();
  const [image, setImage] = useState(false);
  var url = location.state.detail;
  var resourcename = location.state.resourcename;
  const token = localStorage.getItem("token");
  var config = {
    headers: {
      "x-access-token": token,
    },
  };
  const [news, setNews] = useState([]);

  useEffect(() => {
    axios
      .post("http://localhost:3001/news/loadresource", { url }, config)
      .then((response) => {
        console.log(response.data);
        const array = response.data.sort((a, b) => a.isoDate > b.isoDate);
        try {
          const value = typeof array[0].enclosure.url;
          setImage(true);
          console.log(array[0].enclosure.url);
        } catch (e) {
          console.log("no hay imagen");
        }

        setNews(array);
      }); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return news.length === 0 ? (
    <div className="watchmain d-flex flex-row justify-content-center align-items-center">
      <div className="divimage"></div>
      <div className="watchframework border">
        <div className="rounded">
          <h4 className="text-center m-3">Nombre de la noticia</h4>
        </div>
        <div className="buttondiv border m-1">
          <button
            className="backbutton text-center m-1"
            onClick={() => {
              history.push("/portal");
            }}
          >
            Back
          </button>
        </div>
        <div className="d-flex flex-row justify-content-center align-items-center mt-5">
          <Loading />
        </div>
      </div>
    </div>
  ) : (
    <Fragment>
      <div className="watchmain d-flex flex-row justify-content-center align-items-center">
        <div className="divimage"></div>
        <div className="watchframework border">
          <div className="rounded">
            <h4 className="text-center m-3">{resourcename}</h4>
          </div>
          <div className="buttondiv border m-1">
            <button
              className="backbutton text-center m-1"
              onClick={() => {
                history.push("/portal");
              }}
            >
              Back
            </button>
          </div>
          <div className="scrolldiv">
            {news.map((item) => (
              <div key={item.content} className="card m-1 d-flex flex-row  ">
                {image ? (
                  <div className=" align-self-center">
                    <a href={item.link}>
                      <img
                        src={item.enclosure.url}
                        className="newsimage "
                        alt="..."
                      />
                    </a>
                  </div>
                ) : (
                  <div className=" align-self-center">
                    <a href={item.link}>
                      <img src={img} className="newsimage" alt="..." />
                    </a>
                  </div>
                )}

                <div className="card-body">
                  <div className="newsmain">
                    <h3 className="text-center">{item.title}</h3>
                    <p className="card-text">{item.content}</p>
                    <a href={item.link}>
                      <button className="linkbutton">Link</button>
                    </a>
                    <h4> date</h4>
                    <p className="card-text">{item.pubDate}</p>
                    <h4>category</h4>
                    <p className="card-text">{item.categories[0]}</p>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
}
