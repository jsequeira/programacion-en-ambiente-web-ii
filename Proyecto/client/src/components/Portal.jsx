import { React, Fragment, useEffect, useState } from "react";
import * as mdb from "mdb-ui-kit"; // lib
import "./css/style.css";
import axios from "axios";
import img from "./img/no-image.svg";
import Myresources from "./Myresources";
import MainNews from "./MainNews";
import Loading from "./Loading";
function Portal() {
  const [newsList, setNewsList] = useState([]);
  const token = localStorage.getItem("token");
  const user = localStorage.getItem("userInfo");
  const jsonUser = JSON.parse(user);
  const userid = jsonUser._id;
  var config = {
    headers: {
      "x-access-token": token,
    },
  };

  useEffect(() => {
    axios
      .post("http://localhost:3001/news/getnewsbyuserid", { userid })
      .then((response) => {
        if (response.data.length === 0) {
          alert("no hay noticias cargadas");
          axios
            .post("http://localhost:3001/news/loadnewstodatabase", { userid })
            .then((response) => {
              alert(response.data);
              axios
                .post("http://localhost:3001/news/loadnewstodatabase", {
                  userid,
                })
                .then((response) => {
                  setNewsList(response.data);
                  alert(response.data.length);
                });
            });
        } else {
          setNewsList(response.data);
          alert(response.data.length);
          const interval = setInterval(async () => {
            console.log("30 segundos");
            await axios
              .post("http://localhost:3001/news/updatedatabasebyid", { userid })
              .then((response) => {
                console.log(response.data);
              });
          }, 1000 * 30);
        }
      });
  }, []);
  return newsList.length === 0 ? (
    <div className="portalmain d-flex flex-row justify-content-center align-items-center">
      <div>
        <h3>wait please...</h3>
        <Loading />
      </div>
    </div>
  ) : (
    <Fragment>
      <div className="portalmain d-flex flex-row justify-content-center align-items-center">
        <div className="divimage"></div>
        <div className="portalframework border">
          <ul className="nav nav-tabs mb-3 m-3" id="ex1" role="tablist">
            <li className="nav-item  " role="presentation">
              <a
                className="nav-link active "
                id="ex1-tab-1"
                data-mdb-toggle="tab"
                href="#ex1-tabs-1"
                role="tab"
                aria-controls="ex1-tabs-1"
                aria-selected="true"
              >
                My resources
              </a>
            </li>
            <li className="nav-item" role="presentation">
              <a
                className="nav-link"
                id="ex1-tab-2"
                data-mdb-toggle="tab"
                href="#ex1-tabs-2"
                role="tab"
                aria-controls="ex1-tabs-2"
                aria-selected="false"
              >
                Main
              </a>
            </li>
            <li className="nav-item" role="presentation">
              <a
                className="nav-link"
                id="ex1-tab-3"
                data-mdb-toggle="tab"
                href="#ex1-tabs-3"
                role="tab"
                aria-controls="ex1-tabs-3"
                aria-selected="false"
              >
                Tab 3
              </a>
            </li>
          </ul>

          <div className="MainNews tab-content" id="ex1-content">
            <Myresources />

            <div
              className="tab-pane fade"
              id="ex1-tabs-2"
              role="tabpanel"
              aria-labelledby="ex1-tab-2"
            >
              <div className="mainframework">
                <div className="scrollmainnews border">
                  {newsList.map((item) => (
                    <div
                      key={item.content}
                      className="card m-1 d-flex flex-row  "
                    >
                      {item.image == "" ? (
                        <div className=" align-self-center">
                          <a href={item.link}>
                            <img src={img} className="newsimage " alt="..." />
                          </a>
                        </div>
                      ) : (
                        <div className=" align-self-center">
                          <a href={item.link}>
                            <img
                              src={item.image}
                              className="newsimage"
                              alt="..."
                            />
                          </a>
                        </div>
                      )}

                      <div className="card-body">
                        <div className="newsmain">
                          <h3 className="text-center">{item.title}</h3>
                          <p className="card-text">{item.description}</p>
                          <a href={item.link}>
                            <button className="linkbutton mt-2 mb-2">
                              Link
                            </button>
                          </a>
                          <h4> date</h4>
                          <p className="card-text">{item.date}</p>
                          <h4>category</h4>
                          <p className="card-text">{item.itemcategory}</p>{" "}
                          <h4>Source</h4>
                          <p className="card-text">{item.source}</p>{" "}
                          <h4>Creator</h4>
                          <p className="card-text">{item.creator}</p>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default Portal;
