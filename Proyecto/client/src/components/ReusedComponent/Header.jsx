import { Fragment, React } from "react";
import logo from "./img/logo.ico";
import { Navbar, Nav, Dropdown } from "react-bootstrap";

import "./css/Header.css";
export default function Header() {
  const url = new URL(window.location.href);
  console.log(url);
  return (
    //no poner border a header ni a main ni a footer
    <Fragment>
      <header>
        <Navbar className="navbar border" collapseOnSelect expand="lg">
          <Navbar.Brand>
            <Nav.Link className="Brand-link " href="/">
              <img src={logo} alt="" />
            </Nav.Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav  " />
          <Navbar.Collapse
            id="basic-navbar-nav "
            className="justify-content-end "
          >
            <Nav>
              <HeaderType />
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </header>
    </Fragment>
  );
}
function HeaderType() {
  const url = new URL(window.location.href);
  if (url.pathname === "/") {
    return (
      <Fragment>
        <Nav.Link className="link m-1 mr-4 " href="/login">
          Log in
        </Nav.Link>
        <Nav.Link className="link  m-1 mr-4 " href="/signup">
          Sign in
        </Nav.Link>
      </Fragment>
    );
  }
  if (url.pathname === "/signup") {
    return (
      <Fragment>
        <Nav.Link className="link m-1 mr-4 " href="/login">
          Log in
        </Nav.Link>
      </Fragment>
    );
  }
  if (url.pathname === "/login") {
    return (
      <Fragment>
        <Nav.Link className="link m-1 mr-4 " href="/login">
          Log in
        </Nav.Link>
        <Nav.Link className="link  m-1 mr-4 " href="/signup">
          Sign in
        </Nav.Link>
      </Fragment>
    );
  } else {
    return (
      <Fragment>
        <Dropdown drop="left" className="navdropdownbutton  mr-4">
          <Dropdown.Menu>
            <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
            <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
            <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Fragment>
    );
  }
}
